var express = require('express');
var router = express.Router();

// Models
var Products = require('../models/product');

// Routes
Products.methods(['get', 'post', 'put', 'delete']);
Products.register(router, '/products');

module.exports = router;